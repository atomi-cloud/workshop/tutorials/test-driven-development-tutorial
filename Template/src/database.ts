import * as fs from "fs";
import * as path from "path";

class Database {

    private readonly file: string;
    private inMem: { [s: string]: any };

    constructor(file: string) {
        this.file = file;
    }

    get Target(): string {
        return path.resolve(__dirname, this.file);
    }

    Create(): void {
        if (this.exist()) {
            throw new Error("Database already exist!");
        } else {
            this.inMem = {};
            this.save();
        }
    }


    Delete(): void {
        if (this.exist()) {
            fs.unlinkSync(this.Target)
        }
    }

    Exist(): boolean {
        return this.exist();
    }

    private exist(): boolean {
        return fs.existsSync(this.Target);
    }

    private save(): void {
        const serialized = JSON.stringify(this.inMem);
        fs.writeFileSync(this.Target, serialized, 'utf8');
    }

    private load(): void {
        const data: string = fs.readFileSync(this.Target, 'utf8');
        this.inMem = JSON.parse(data);
    }

    Write(key: string, value: any) {
        if (!this.exist()) {
            throw new Error("No such database");
        }
        this.inMem[key] = value;
        this.save();
    }

    Read(key: string): any {
        if (!this.exist()) {
            throw new Error("No such database");
        }
        this.load();
        const value = fs.readFileSync(this.Target, 'utf8');
        this.inMem = JSON.parse(value);
        return this.inMem[key];
    }
}

export {Database}