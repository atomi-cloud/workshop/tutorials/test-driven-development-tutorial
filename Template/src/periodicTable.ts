import {Atom} from "./Atom";

const hydrogen = new Atom("Hydrogen", 1, 1.008, "H", [1]);
const helium = new Atom("Helium", 2, 4.0026, "He", [2]);
const lithium = new Atom("Lithium", 3, 6.94, "Li", [2, 1]);
const beryllium = new Atom("Beryllium", 4, 9.0122, "Be", [2, 2]);
const boron = new Atom("Boron", 5, 10.81, "B", [2, 3]);
const carbon = new Atom("Carbon", 6, 12.011, "C", [2, 4]);
const nitrogen = new Atom("Nitrogen", 7, 14.0007, "N", [2, 5]);
const oxygen = new Atom("Oxygen", 8, 15.999, "O", [2, 6]);
const fluorine = new Atom("Fluorine", 9, 18.998, "F", [2, 7]);
const neon = new Atom("Neon", 10, 20.180, "Ne", [2, 8]);
const sodium = new Atom("Sodium", 11, 22.990, "Na", [2, 8, 1]);
const magnesium = new Atom("Magnesium", 12, 24.305, "Mg", [2, 8, 2]);
const aluminium = new Atom("Aluminium", 13, 26.982, "Al", [2, 8, 3]);
const silicon = new Atom("Silicon", 14, 28.085, "Si", [2, 8, 4]);
const phosphorus = new Atom("Phosphorus", 15, 30.974, "P", [2, 8, 5]);
const sulfur = new Atom("Sulfur", 16, 32.06, "S", [2, 8, 6]);
const chlorine = new Atom("Chlorine", 17, 35.45, "Cl", [2, 8, 7]);
const argon = new Atom("Argon", 18, 39.948, "Ar", [2, 8, 8]);
const potassium = new Atom("Potassium", 19, 39.098, "K", [2, 8, 8, 1]);


export {
    hydrogen,
    helium,
    lithium,
    beryllium,
    boron,
    carbon,
    nitrogen,
    oxygen,
    fluorine,
    neon,
    sodium,
    magnesium,
    aluminium,
    silicon,
    phosphorus,
    sulfur,
    chlorine,
    argon,
    potassium,
}