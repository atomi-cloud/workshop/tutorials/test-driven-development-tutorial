class Atom {
    fullname: string
    atomicNumber: number
    weight: number
    letter: string
    orbital: number[];

    constructor(fullname: string, atomicNumber: number, weight: number, letter: string, orbital: number[]) {
        this.fullname = fullname;
        this.atomicNumber = atomicNumber;
        this.weight = weight;
        this.letter = letter;
        this.orbital = orbital;
    }
}

export {Atom}