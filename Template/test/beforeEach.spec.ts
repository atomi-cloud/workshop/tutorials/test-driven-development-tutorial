import {should} from "chai"

should();

let x = 6;


function evil(a: number): number {
    x = a + x;
    return x;
}

// beforeEach(()=>{
//     x = 6;
// });

describe('test name', () => {
    it('should do something again', function () {
        const y = evil(5);
        y.should.be.equal(11);
    });

    it('should do something again', function () {
        const y = evil(7);
        y.should.be.equal(13);
    });
});

