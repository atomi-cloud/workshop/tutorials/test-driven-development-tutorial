import {Database} from "../src/database";
import {should} from "chai";

should();

const database = new Database("periodic.db");

describe('State 2', () => {
    // before(() => {
    //     if (database.Exist()) {
    //         database.Delete();
    //     }
    //     database.Create();
    //     database.Write("hydrogen", {
    //         atomicNumber: 1,
    //         letter: "H",
    //         name: "Hydrogen",
    //         orbitals: [1],
    //     })
    // });

    it('should return null if the key is empty', () => {
        const x = database.Read("hydrogen");
        x.atomicNumber.should.equal(1);
    });

    it("should return the correct key after written", () => {
        const x = database.Read("helium");
        (x == null).should.be.true;
    });

    // after(() => {
    //     database.Delete();
    // });
});

