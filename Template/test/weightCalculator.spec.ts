import {should} from "chai"
import {WeightCalculator} from "../src/WeightCalculator";
import {carbon, chlorine, hydrogen, oxygen, sodium} from "../src/periodicTable";

should();


//You can change the constructor
const weightCalculator: WeightCalculator = new WeightCalculator();


describe("weight calculator should sum the weight properly", () => {

    it("should calculate the weight of water properly", () => {
        weightCalculator.Calculate("water").should.equal(hydrogen.weight * 2 + oxygen.weight);
    });

    it('should calculate the weight of salt properly', function () {
        weightCalculator.Calculate('salt').should.equal(sodium.weight + chlorine.weight);
    });

    it('should calculate the weight of carbon dioxide', function () {
        weightCalculator.Calculate('carbon dioxide').should.equal(carbon.weight + oxygen.weight * 2)
    });

    it('should calculate the weight of sugar', function () {
        weightCalculator.Calculate('sugar').should.equal(carbon.weight * 12 + hydrogen.weight * 22 + oxygen.weight * 11)
    });
});

