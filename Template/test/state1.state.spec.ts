import {should} from "chai";
import {Database} from "../src/database";

should();

const database = new Database("periodic.db");


describe('State 1', () => {
    // before(() => {
    //     if (database.Exist()) {
    //         database.Delete();
    //     }
    //     database.Create();
    //     database.Write("helium", {
    //         atomicNumber: 2,
    //         letter: "He",
    //         name: "Helium",
    //         orbitals: [2],
    //     });
    // });

    it('should return null if the key is empty', () => {
        const x = database.Read("hydrogen");
        (x == null).should.be.true;
    });

    it("should return the correct key after written", () => {
        const x = database.Read("helium");
        x.atomicNumber.should.equal(2);
    });

    // after(() => {
    //     database.Delete();
    // });

});

