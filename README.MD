# Test Driven Development Tutorial
Tutorial Repository for test driven development

# Getting Started
1. Download and install [NPM](https://nodejs.org/en/download/) or [Yarn](https://yarnpkg.com/)
2. Install CyanPrint globally via NPM
    ```bash
    $ npm i -g cyanprint@latest
    ```
    
    or Yarn
    ```bash
    $ yarn global add cyanprint@latest 
    ```
   
     
3. Install this template
    ```bash
    $ cyan i tddtut_ts [group]
    ```
4. Create templates from via CyanPrint CLI
    ```bash
    $ cyan create [app name]
    ```
    
# Authors
 - kirinnee
 
# Contributing
Please read [CONTRIBUTING.md](CONTRIBUTING.MD) for details on our code of conduct, and the process for submitting pull requests to us.

# Versioning
We use [SemVer](https://semver.org/) for versioning. 
For the versions available, see the tags on this repository.
 
# License
This template is licensed under MIT - see the [LICENSE.md](LICENSE.MD) file for more details
